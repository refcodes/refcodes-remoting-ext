module org.refcodes.remoting.ext.observer {
	requires org.refcodes.controlflow;
	requires transitive org.refcodes.component.ext.observer;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.observer;
	requires transitive org.refcodes.remoting;
	requires org.refcodes.runtime;

	exports org.refcodes.remoting.ext.observer;
}

// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import org.refcodes.mixin.TypeAccessor;

/**
 * The {@link AbstractTypeEvent} is the base implementation of the
 * {@link ClientEvent} interface with type access.
 */
@SuppressWarnings("rawtypes")
abstract class AbstractTypeEvent extends AbstractRemotingEvent<ClientAction, ObservableRemoteClient> implements ClientEvent, TypeAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Class<?> _type;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link AbstractTypeEvent} event.
	 *
	 * @param aAction The action describing this event.
	 * @param aType the type
	 * @param aSource The according source (origin).
	 */
	public AbstractTypeEvent( ClientAction aAction, Class<?> aType, ObservableRemoteClient aSource ) {
		super( aAction, aSource );
		_type = aType;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<?> getType() {
		return _type;
	}
}

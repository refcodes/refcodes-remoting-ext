// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import java.util.concurrent.ExecutorService;

import org.refcodes.component.ext.observer.ClosedEvent;
import org.refcodes.component.ext.observer.ConnectionEvent;
import org.refcodes.component.ext.observer.OpenedEvent;
import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.exception.VetoException;
import org.refcodes.observer.AbstractObservable;
import org.refcodes.observer.Observable;
import org.refcodes.remoting.RemoteClient;

/**
 * Event enabled (observable) remote control extending the {@link RemoteClient}
 * to be observable.
 */
public class ObservableRemoteClient extends RemoteClient implements Observable<ConsumerRemoteObserver> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ConsumerRemoteObservable _observable;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new observable remote client impl.
	 */
	public ObservableRemoteClient() {
		this( null );
	}

	/**
	 * Instantiates a new observable remote client impl.
	 *
	 * @param aExecutorService the executor service
	 */
	public ObservableRemoteClient( ExecutorService aExecutorService ) {
		super( aExecutorService );
		_observable = new ConsumerRemoteObservable( getExecutorService() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		if ( !isDestroyed() ) {
			super.destroy();
			_observable.clear();
			_observable = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasObserver( ConsumerRemoteObserver aObserver ) {
		ControlFlowUtility.throwIllegalStateException( isDestroyed() );
		return _observable.hasObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean subscribeObserver( ConsumerRemoteObserver aObserver ) {
		ControlFlowUtility.throwIllegalStateException( isDestroyed() );
		return _observable.subscribeObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean unsubscribeObserver( ConsumerRemoteObserver aObserver ) {
		ControlFlowUtility.throwIllegalStateException( isDestroyed() );
		return _observable.unsubscribeObserver( aObserver );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onClosed() {
		if ( !_observable.isEmpty() ) {
			try {
				_observable.fireEvent( new ClosedEvent<>( this ) );
			}
			catch ( VetoException aException ) {
				/* not thrown by observer */
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onOpened() {
		if ( !_observable.isEmpty() ) {
			try {
				_observable.fireEvent( new OpenedEvent<>( this ) );
			}
			catch ( VetoException aException ) {
				/* not thrown by observer */
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onProxyPublished( Object aProxy ) {
		try {
			_observable.fireEvent( new ProxyPublishedEvent( aProxy, this ) );
		}
		catch ( VetoException aException ) {
			/* not thrown by observer */
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onProxySignedOff( Object aProxy ) {
		if ( !_observable.isEmpty() ) {
			final ProxySignedOffEvent theProxySignedOffEvent = new ProxySignedOffEvent( aProxy, this );
			try {
				_observable.fireEvent( theProxySignedOffEvent );
			}
			catch ( VetoException aException ) {
				/* not thrown by observer */
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onPublishProxy( Class<?> aType ) throws VetoException {
		if ( !_observable.isEmpty() ) {
			_observable.fireEvent( new PublishProxyEvent( aType, this ) );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static class ConsumerRemoteObservable extends AbstractObservable<ConsumerRemoteObserver, ConnectionEvent<?, ObservableRemoteClient>> {

		public ConsumerRemoteObservable( ExecutorService aExecutorService ) {
			super( aExecutorService );
		}

		@Override
		public void clear() {
			super.clear();
		}

		@Override
		public boolean isEmpty() {
			return super.isEmpty();
		}

		@Override
		public int size() {
			return super.size();
		}

		protected boolean fireEvent( ConnectionEvent<?, ObservableRemoteClient> aEvent ) throws VetoException {
			return super.fireEvent( aEvent, ExecutionStrategy.PARALLEL );
		}

		@SuppressWarnings("unchecked")
		@Override
		protected boolean fireEvent( ConnectionEvent<?, ObservableRemoteClient> aEvent, ConsumerRemoteObserver aObserver, ExecutionStrategy aEventExecutionStrategy ) throws VetoException {
			if ( aEvent instanceof PublishProxyEvent ) {
				aObserver.onPublishProxyEvent( (PublishProxyEvent) aEvent );
			}
			else if ( aEvent instanceof ProxyPublishedEvent ) {
				aObserver.onProxyPublishedEvent( (ProxyPublishedEvent) aEvent );
			}
			else if ( aEvent instanceof SignOffProxyEvent ) {
				aObserver.onSignOffProxyEvent( (SignOffProxyEvent) aEvent );
			}
			else if ( aEvent instanceof ProxySignedOffEvent ) {
				aObserver.onProxySignedOffEvent( (ProxySignedOffEvent) aEvent );
			}
			else if ( aEvent instanceof OpenedEvent ) {
				aObserver.onOpendEvent( (OpenedEvent<ObservableRemoteClient>) aEvent );
			}
			else if ( aEvent instanceof ClosedEvent ) {
				aObserver.onClosedEvent( (ClosedEvent<ObservableRemoteClient>) aEvent );
			}
			aObserver.onEvent( (ConnectionEvent<Enum<?>, ObservableRemoteClient>) aEvent );
			return true;
		}
	}
}

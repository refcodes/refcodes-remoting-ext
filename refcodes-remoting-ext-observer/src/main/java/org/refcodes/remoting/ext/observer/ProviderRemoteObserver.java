// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import org.refcodes.component.ext.observer.ConnectionObserver;
import org.refcodes.observer.ActionEvent;
import org.refcodes.observer.Observer;
import org.refcodes.remoting.RemoteServer;

/**
 * An {@link Observer} for {@link RemoteServer} related {@link ActionEvent}
 * instances.
 */
public interface ProviderRemoteObserver extends ConnectionObserver<ObservableRemoteServer> {

	/**
	 * Signaled in case a subject has been published.
	 * 
	 * @param aSubjectPublishedEvent The {@link SubjectPublishedEvent} providing
	 *        information on the subject being published.
	 */
	void onSubjectPublishedEvent( SubjectPublishedEvent aSubjectPublishedEvent );

	/**
	 * Signaled in case a subject has been signed-off.
	 * 
	 * @param aSubjectSignedOffEvent The {@link SubjectSignedOffEvent} providing
	 *        information on the subject being signed-off.
	 */
	void onSubjectSignedOffEvent( SubjectSignedOffEvent aSubjectSignedOffEvent );
}

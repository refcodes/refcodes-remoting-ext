// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import org.refcodes.remoting.SubjectAccessor;

/**
 * The {@link AbstractSubjectEvent} is the base implementation of the
 * {@link ServerEvent} interface with subject access.
 */
abstract class AbstractSubjectEvent extends AbstractRemotingEvent<ServerAction, ObservableRemoteServer> implements ServerEvent, SubjectAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Object _subject;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link AbstractSubjectEvent} event.
	 *
	 * @param aAction The action describing this event.
	 * @param aSubject the subject
	 * @param aSource The according source (origin).
	 */
	public AbstractSubjectEvent( ServerAction aAction, Object aSubject, ObservableRemoteServer aSource ) {
		super( aAction, aSource );
		_subject = aSubject;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Gets the subject.
	 *
	 * @param <S> the generic type
	 * 
	 * @return the subject
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <S> S getSubject() {
		return (S) _subject;
	}
}

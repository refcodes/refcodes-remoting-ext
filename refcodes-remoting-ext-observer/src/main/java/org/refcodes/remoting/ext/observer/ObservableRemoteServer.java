// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import java.util.concurrent.ExecutorService;

import org.refcodes.component.ext.observer.ClosedEvent;
import org.refcodes.component.ext.observer.ConnectionEvent;
import org.refcodes.component.ext.observer.OpenedEvent;
import org.refcodes.controlflow.ControlFlowUtility;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.exception.VetoException;
import org.refcodes.observer.AbstractObservable;
import org.refcodes.observer.Observable;
import org.refcodes.remoting.RemoteServer;

/**
 * Event enabled (observable) remote control extending the {@link RemoteServer}
 * to be observable.
 */
public class ObservableRemoteServer extends RemoteServer implements Observable<ProviderRemoteObserver> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ProviderRemoteObservable _observable;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new observable remote server impl.
	 */
	public ObservableRemoteServer() {
		this( null );
	}

	/**
	 * Instantiates a new observable remote server impl.
	 *
	 * @param aExecutorService the executor service
	 */
	public ObservableRemoteServer( ExecutorService aExecutorService ) {
		super( aExecutorService );
		_observable = new ProviderRemoteObservable( getExecutorService() );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		if ( !isDestroyed() ) {
			super.destroy();
			_observable.clear();
			_observable = null;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasObserver( ProviderRemoteObserver aObserver ) {
		ControlFlowUtility.throwIllegalStateException( isDestroyed() );
		return _observable.hasObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean subscribeObserver( ProviderRemoteObserver aObserver ) {
		ControlFlowUtility.throwIllegalStateException( isDestroyed() );
		return _observable.subscribeObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean unsubscribeObserver( ProviderRemoteObserver aObserver ) {
		ControlFlowUtility.throwIllegalStateException( isDestroyed() );
		return _observable.unsubscribeObserver( aObserver );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onClosed() {
		if ( !_observable.isEmpty() ) {
			try {
				_observable.fireEvent( new ClosedEvent<>( this ) );
			}
			catch ( VetoException aException ) {
				/* not thrown by observer */
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onOpened() {
		if ( !_observable.isEmpty() ) {
			try {
				_observable.fireEvent( new OpenedEvent<>( this ) );
			}
			catch ( VetoException aException ) {
				/* not thrown by observer */
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onSubjectPublished( Object aSubject ) {
		if ( !_observable.isEmpty() ) {
			try {
				_observable.fireEvent( new SubjectPublishedEvent( aSubject, this ) );
			}
			catch ( VetoException aException ) {
				/* not thrown by observer */
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onSubjectSignedOff( Object aSubject ) {
		if ( !_observable.isEmpty() ) {
			final SubjectSignedOffEvent eSubjectSignedOffEvent = new SubjectSignedOffEvent( aSubject, this );
			try {
				_observable.fireEvent( eSubjectSignedOffEvent );
			}
			catch ( VetoException ve ) {
				/* not thrown by observer */
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static class ProviderRemoteObservable extends AbstractObservable<ProviderRemoteObserver, ConnectionEvent<?, ObservableRemoteServer>> {

		public ProviderRemoteObservable( ExecutorService aExecutorService ) {
			super( aExecutorService );
		}

		@Override
		public void clear() {
			super.clear();
		}

		@Override
		public boolean isEmpty() {
			return super.isEmpty();
		}

		@Override
		public int size() {
			return super.size();
		}

		protected boolean fireEvent( ConnectionEvent<?, ObservableRemoteServer> aEvent ) throws VetoException {
			return super.fireEvent( aEvent, ExecutionStrategy.PARALLEL );
		}

		@Override
		@SuppressWarnings("unchecked")
		protected boolean fireEvent( ConnectionEvent<?, ObservableRemoteServer> aEvent, ProviderRemoteObserver aObserver, ExecutionStrategy aEventExecutionStrategy ) throws VetoException {
			if ( aEvent instanceof SubjectPublishedEvent ) {
				aObserver.onSubjectPublishedEvent( (SubjectPublishedEvent) aEvent );
			}
			else if ( aEvent instanceof SubjectSignedOffEvent ) {
				aObserver.onSubjectSignedOffEvent( (SubjectSignedOffEvent) aEvent );
			}
			else if ( aEvent instanceof OpenedEvent ) {
				aObserver.onOpendEvent( (OpenedEvent<ObservableRemoteServer>) aEvent );
			}
			else if ( aEvent instanceof ClosedEvent ) {
				aObserver.onClosedEvent( (ClosedEvent<ObservableRemoteServer>) aEvent );
			}
			aObserver.onEvent( (ConnectionEvent<Enum<?>, ObservableRemoteServer>) aEvent );
			return true;
		}
	}
}

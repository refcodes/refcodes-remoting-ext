// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import org.refcodes.observer.ActionEvent;
import org.refcodes.remoting.ProxyAccessor;

/**
 * The {@link AbstractProxyEvent} is the base implementation of the
 * {@link ClientEvent} interface with proxy access.
 */
abstract class AbstractProxyEvent extends AbstractRemotingEvent<ClientAction, ObservableRemoteClient> implements ClientEvent, ProxyAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Object _proxy;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link ActionEvent} providing a proxy {@link Object} as
	 * argument.
	 *
	 * @param aAction The action describing this event.
	 * @param aProxy The proxy to be provided by the {@link ActionEvent}.
	 * @param aSource The source from which this event originated.
	 */
	public AbstractProxyEvent( ClientAction aAction, Object aProxy, ObservableRemoteClient aSource ) {
		super( aAction, aSource );
		_proxy = aProxy;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <P> P getProxy() {
		return (P) _proxy;
	}
}

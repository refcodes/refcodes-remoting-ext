// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import org.refcodes.component.ext.observer.ClosedEvent;
import org.refcodes.component.ext.observer.ConnectionEvent;
import org.refcodes.component.ext.observer.OpenedEvent;
import org.refcodes.exception.VetoException;

/**
 * The {@link AbstractConsumerObserver} is an abstract implementation of the
 * {@link ProviderRemoteObserver} for convenience reasons. Any sub-classes just
 * require to override the required methods.
 */
abstract class AbstractConsumerObserver implements ConsumerRemoteObserver {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onClosedEvent( ClosedEvent<ObservableRemoteClient> aEvent ) {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onEvent( ConnectionEvent<?, ObservableRemoteClient> aEvent ) {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onOpendEvent( OpenedEvent<ObservableRemoteClient> aEvent ) {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onProxyPublishedEvent( ProxyPublishedEvent aProxyPublishedEvent ) {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onProxySignedOffEvent( ProxySignedOffEvent aProxySignedOffEvent ) {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onPublishProxyEvent( PublishProxyEvent aPublishProxyEvent ) throws VetoException {}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void onSignOffProxyEvent( SignOffProxyEvent aSignOffProxyEvent ) throws VetoException {}
}

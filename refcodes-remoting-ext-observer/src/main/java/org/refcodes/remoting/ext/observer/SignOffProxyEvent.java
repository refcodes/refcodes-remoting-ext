// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import org.refcodes.mixin.TimeoutMillisAccessor;
import org.refcodes.observer.ActionEvent;

/**
 * The {@link SignOffProxyEvent} signals a proxy to be signed off.
 */
public class SignOffProxyEvent extends AbstractProxyEvent implements TimeoutMillisAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final long _timeoutMillis;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an according {@link SignOffProxyEvent} event.
	 *
	 * @param aProxy The proxy to be provided by the {@link ActionEvent}.
	 * @param aTimeoutMillis The timeout in milliseconds after which the proxy
	 *        will finally be signed-off.
	 * @param aSource The source from which this event originated.
	 */
	public SignOffProxyEvent( Object aProxy, long aTimeoutMillis, ObservableRemoteClient aSource ) {
		super( ClientAction.SIGNOFF_PROXY, aProxy, aSource );
		_timeoutMillis = aTimeoutMillis;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public long getTimeoutMillis() {
		return _timeoutMillis;
	}
}

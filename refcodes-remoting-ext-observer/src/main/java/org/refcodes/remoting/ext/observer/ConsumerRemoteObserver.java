// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import org.refcodes.component.ext.observer.ConnectionObserver;
import org.refcodes.exception.VetoException;

/**
 * An asynchronous update interface for receiving notifications about
 * ConsumerRemote information as the ConsumerRemote is constructed.
 */
public interface ConsumerRemoteObserver extends ConnectionObserver<ObservableRemoteClient> {

	/**
	 * This method is called when information about an ConsumerRemote which was
	 * previously requested using an asynchronous interface becomes available.
	 *
	 * @param aProxyPublishedEvent the proxy published event
	 */
	void onProxyPublishedEvent( ProxyPublishedEvent aProxyPublishedEvent );

	/**
	 * This method is called when information about an ConsumerRemote which was
	 * previously requested using an asynchronous interface becomes available.
	 *
	 * @param aProxySignedOffEvent the proxy signed off event
	 */
	void onProxySignedOffEvent( ProxySignedOffEvent aProxySignedOffEvent );

	/**
	 * This method is called when information about an ConsumerRemote which was
	 * previously requested using an asynchronous interface becomes available.
	 *
	 * @param aPublishProxyEvent the publish proxy event
	 * 
	 * @throws VetoException the veto exception
	 */
	void onPublishProxyEvent( PublishProxyEvent aPublishProxyEvent ) throws VetoException;

	/**
	 * This method is called when information about an ConsumerRemote which was
	 * previously requested using an asynchronous interface becomes available.
	 *
	 * @param aSignOffProxyEvent the sign off proxy event
	 * 
	 * @throws VetoException the veto exception
	 */
	void onSignOffProxyEvent( SignOffProxyEvent aSignOffProxyEvent ) throws VetoException;
}

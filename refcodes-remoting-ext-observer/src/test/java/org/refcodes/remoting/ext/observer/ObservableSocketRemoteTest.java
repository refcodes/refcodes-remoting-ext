// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.component.ext.observer.ClosedEvent;
import org.refcodes.component.ext.observer.ConnectionEvent;
import org.refcodes.exception.Trap;
import org.refcodes.io.PrefetchBidirectionalStreamConnectionTransceiver;
import org.refcodes.remoting.AmbiguousProxyException;
import org.refcodes.remoting.NoSuchProxyException;
import org.refcodes.remoting.RemoteClient;
import org.refcodes.remoting.RemoteServer;
import org.refcodes.runtime.SystemProperty;

/**
 * Test suite for the {@link IoStreamConsumerRemoteImpl} and
 * {@link IoStreamProviderRemoteImpl} class of {@link RemoteClient} and
 * {@link RemoteServer} implementations.
 */
public class ObservableSocketRemoteTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int PORT = 5161;
	private static final int ITERARTIONS = 1000;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////
	PrefetchBidirectionalStreamConnectionTransceiver<Serializable> _serverTransceiver = new PrefetchBidirectionalStreamConnectionTransceiver<>();
	ObservableRemoteServer _server = new ObservableRemoteServer();
	boolean _hasProviderSocket = false;

	/**
	 * A normal test for the {@link IoStreamProviderRemoteImpl} and
	 * {@link IoStreamConsumerRemoteImpl} class of remoting implementations.
	 *
	 * @throws UnknownHostException the unknown host exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws IOException the open exception
	 */
	@Test
	public void testObservableIoStreamRemote() throws IOException {
		final PrefetchBidirectionalStreamConnectionTransceiver<Serializable> theClientTransceiver = new PrefetchBidirectionalStreamConnectionTransceiver<>();
		final ObservableRemoteClient theRemoteClient = new ObservableRemoteClient();
		theRemoteClient.subscribeObserver( new AbstractConsumerObserver() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onEvent( ConnectionEvent<?, ObservableRemoteClient> aEvent ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Event = " + aEvent.getClass().getName() );
				}
				if ( aEvent instanceof ProxyPublishedEvent theProxyEvent ) {
					final Object theProxy = theProxyEvent.getProxy();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Proxy class = " + theProxy.getClass().getName() );
					}
					if ( theProxy instanceof List ) {
						// Just to see if #getPrxy() funktions:
						try {
							@SuppressWarnings({ "unchecked" })
							final List<String> myList = theRemoteClient.getProxy( List.class );
							if ( SystemProperty.LOG_TESTS.isEnabled() ) {
								System.out.println( Arrays.toString( myList.toArray() ) );
							}
						}
						catch ( Exception e ) {
							e.printStackTrace();
						}
						final List<String> theProxyList = theProxyEvent.getProxy();
						theProxyList.add( "1" );
						theProxyList.add( "2" );
						theProxyList.add( "3" );
						synchronized ( ObservableSocketRemoteTest.this ) {
							ObservableSocketRemoteTest.this.notifyAll();
						}
					}
				}
				else if ( aEvent instanceof ClosedEvent ) {
					synchronized ( ObservableSocketRemoteTest.this ) {
						ObservableSocketRemoteTest.this.notifyAll();
					}
				}
			};
		} );
		final Thread theThread = new Thread( new ProviderSocketDaemon() );
		theThread.start();
		do {
			try {
				// Wait at least one iteration till server socket is ready:
				Thread.sleep( 100 );
			}
			catch ( InterruptedException ignored ) {}
		} while ( !_hasProviderSocket );
		final Socket theClientSocket = new Socket( "localhost", PORT );
		theClientTransceiver.open( theClientSocket.getInputStream(), theClientSocket.getOutputStream() );
		theRemoteClient.open( theClientTransceiver );
		while ( !_server.isOpened() ) {
			try {
				Thread.sleep( 100 );
			}
			catch ( InterruptedException ignored ) {}
		}
		final CountingList<String> theList = new CountingList<>();
		theList.add( "A" );
		theList.add( "B" );
		theList.add( "C" );
		assertEquals( 3, theList.getAddCount() );
		Iterator<String> e = theList.iterator();
		String ePublished;
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Before published = " + ePublished );
			}
		}
		_server.publishSubject( theList );
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ie ) {}
		}
		e = theList.iterator();
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "After published = " + ePublished );
			}
		}
		assertTrue( theList.contains( "1" ) );
		assertTrue( theList.contains( "2" ) );
		assertTrue( theList.contains( "3" ) );
		assertTrue( theList.contains( "A" ) );
		assertTrue( theList.contains( "B" ) );
		assertTrue( theList.contains( "C" ) );
		assertEquals( 6, theList.getAddCount() );
		_server.close();
		synchronized ( this ) {
			try {
				this.wait( 100 );
			}
			catch ( InterruptedException e1 ) {}
		}
		theRemoteClient.close();
		theClientSocket.close();
	}

	@Test
	public void testStressObservableIoStreamRemote() throws IOException {
		final PrefetchBidirectionalStreamConnectionTransceiver<Serializable> theClientTransceiver = new PrefetchBidirectionalStreamConnectionTransceiver<>();
		final ObservableRemoteClient theRemoteClient = new ObservableRemoteClient();
		theRemoteClient.subscribeObserver( new AbstractConsumerObserver() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onEvent( ConnectionEvent<?, ObservableRemoteClient> aEvent ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Event = " + aEvent.getClass().getName() );
				}
				if ( aEvent instanceof ProxyPublishedEvent theProxyEvent ) {
					final Object theProxy = theProxyEvent.getProxy();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Proxy class = " + theProxy.getClass().getName() );
					}
					if ( theProxy instanceof List ) {
						final List<String> theProxyList = theProxyEvent.getProxy();
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( "Perforimg ADD operations:" );
						}
						for ( int i = 0; i < ITERARTIONS; i++ ) {
							if ( i % 10 == 0 ) {
								if ( SystemProperty.LOG_TESTS.isEnabled() ) {
									System.out.println( "ADD(" + i + ")" );
								}
							}
							theProxyList.add( Integer.toString( i ) );
							assertTrue( theProxyList.contains( Integer.toString( i ) ) );
						}
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( "Perforimg REMOVE operations:" );
						}
						for ( int i = 0; i < ITERARTIONS; i++ ) {
							if ( i % 10 == 0 ) {
								if ( SystemProperty.LOG_TESTS.isEnabled() ) {
									System.out.println( "REMOVE(" + i + ")" );
								}
							}
							theProxyList.remove( Integer.toString( i ) );
							assertFalse( theProxyList.contains( Integer.toString( i ) ) );
						}
						assertFalse( theProxyList.isEmpty() );
						assertTrue( theProxyList.contains( "A" ) );
						assertTrue( theProxyList.contains( "B" ) );
						assertTrue( theProxyList.contains( "C" ) );
						theProxyList.clear();
						assertTrue( theProxyList.isEmpty() );
						synchronized ( ObservableSocketRemoteTest.this ) {
							ObservableSocketRemoteTest.this.notifyAll();
						}
					}
				}
				else if ( aEvent instanceof ClosedEvent ) {
					synchronized ( ObservableSocketRemoteTest.this ) {
						ObservableSocketRemoteTest.this.notifyAll();
					}
				}
			};
		} );
		final Thread theThread = new Thread( new ProviderSocketDaemon() );
		theThread.start();
		do {
			try {
				// Wait at least one iteration till server socket is ready:
				Thread.sleep( 100 );
			}
			catch ( InterruptedException ignored ) {}
		} while ( !_hasProviderSocket );
		final Socket theClientSocket = new Socket( "localhost", PORT );
		theClientTransceiver.open( theClientSocket.getInputStream(), theClientSocket.getOutputStream() );
		theRemoteClient.open( theClientTransceiver );
		while ( !_server.isOpened() ) {
			try {
				Thread.sleep( 100 );
			}
			catch ( InterruptedException ignored ) {}
		}
		final CountingList<String> theList = new CountingList<>();
		theList.add( "A" );
		theList.add( "B" );
		theList.add( "C" );
		assertEquals( 3, theList.getAddCount() );
		Iterator<String> e = theList.iterator();
		String ePublished;
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Before published = " + ePublished );
			}
		}
		_server.publishSubject( theList );
		synchronized ( this ) {
			try {
				wait();
			}
			catch ( InterruptedException ie ) {}
		}
		e = theList.iterator();
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "After published = " + ePublished );
			}
		}
		assertTrue( theList.isEmpty() );
		assertEquals( ITERARTIONS + 3, theList.getAddCount() );
		assertEquals( ITERARTIONS, theList.getRemoveCount() );
		assertEquals( 1, theList.getClearCount() );
		_server.close();
		synchronized ( this ) {
			try {
				this.wait( 100 );
			}
			catch ( InterruptedException e1 ) {}
		}
		theRemoteClient.close();
		theClientSocket.close();
	}

	// /////////////////////////////////////////////////////////////////////////
	// SAMPLES:
	// /////////////////////////////////////////////////////////////////////////

	@SuppressWarnings({ "resource", "unused" })
	private void sampleServerCode() throws IOException {
		final Object anyInstance = new Object();
		// ...
		final RemoteServer theServer = new RemoteServer();
		theServer.publishSubject( anyInstance );
		// ...
		final ServerSocket theServerSocket = new ServerSocket( PORT );
		final Socket theSocket = theServerSocket.accept();
		final PrefetchBidirectionalStreamConnectionTransceiver<Serializable> theServerTransceiver = new PrefetchBidirectionalStreamConnectionTransceiver<>();
		theServerTransceiver.open( theSocket.getInputStream(), theSocket.getOutputStream() );
		theServer.open( theServerTransceiver );
	}

	@SuppressWarnings({ "resource", "unused" })
	private void sampleClientCode() throws IOException, AmbiguousProxyException, NoSuchProxyException {
		final RemoteClient theClient = new RemoteClient();
		final Socket theClientSocket = new Socket( "localhost", PORT );
		final PrefetchBidirectionalStreamConnectionTransceiver<Serializable> theClientTransceiver = new PrefetchBidirectionalStreamConnectionTransceiver<>();
		theClientTransceiver.open( theClientSocket.getInputStream(), theClientSocket.getOutputStream() );
		theClient.open( theClientTransceiver );
		// ...
		final Iterator<Object> theProxies = theClient.proxies();
		if ( theClient.hasProxy( List.class ) ) {
			final List<?> myList = theClient.getProxy( List.class );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class ProviderSocketDaemon implements Runnable {

		@Override
		public void run() {
			try ( ServerSocket theServerSocket = new ServerSocket( PORT ) ) {
				_hasProviderSocket = true;
				final Socket theSocket = theServerSocket.accept();
				_serverTransceiver.open( theSocket.getInputStream(), theSocket.getOutputStream() );
				_server.open( _serverTransceiver );
			}
			catch ( IOException e ) {
				System.err.println( Trap.asMessage( e ) );
				e.printStackTrace();
			}
		}
	}

	public class CountingList<T> extends ArrayList<T> implements List<T>, Serializable {
		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private int _addCount = 0;
		private int _removeCount = 0;
		private int _clearCount = 0;

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		@Override
		public boolean add( T o ) {
			final boolean isAdd = super.add( o );
			_addCount++;
			return isAdd;
		}

		@Override
		public void clear() {
			super.clear();
			_clearCount++;
		}

		@Override
		public boolean remove( Object o ) {
			final boolean isRemove = super.remove( o );
			_removeCount++;
			return isRemove;
		}
		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		public int getAddCount() {
			return _addCount;
		}

		public int getRemoveCount() {
			return _removeCount;
		}

		public int getClearCount() {
			return _clearCount;
		}
	}
}

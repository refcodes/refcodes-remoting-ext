// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.remoting.ext.observer;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.component.ext.observer.ClosedEvent;
import org.refcodes.component.ext.observer.ConnectionEvent;
import org.refcodes.io.LoopbackDatagramsTransceiver;
import org.refcodes.runtime.SystemProperty;

public class ObservableLoopbackRemoteTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int ITERARTIONS = 1000;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _listEvent = false;
	private boolean _closedEvent = false;

	@Test
	public void testObservableLoopbackRemote() throws IOException {
		final LoopbackDatagramsTransceiver<Serializable> theClientTransceiver = new LoopbackDatagramsTransceiver<>();
		final LoopbackDatagramsTransceiver<Serializable> theServerTransceiver = new LoopbackDatagramsTransceiver<>();
		final ObservableRemoteServer theRemoteServer = new ObservableRemoteServer();
		final ObservableRemoteClient theRemoteClient = new ObservableRemoteClient();
		theRemoteClient.subscribeObserver( new AbstractConsumerObserver() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onEvent( ConnectionEvent<?, ObservableRemoteClient> aEvent ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Event = " + aEvent.getClass().getName() );
				}
				if ( aEvent instanceof ProxyPublishedEvent theProxyEvent ) {
					final Object theProxy = theProxyEvent.getProxy();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Proxy class = " + theProxy.getClass().getName() );
					}
					if ( theProxy instanceof List ) {
						final List<String> theProxyList = theProxyEvent.getProxy();
						theProxyList.add( "1" );
						theProxyList.add( "2" );
						theProxyList.add( "3" );
						_listEvent = true;
						synchronized ( ObservableLoopbackRemoteTest.this ) {
							ObservableLoopbackRemoteTest.this.notifyAll();
						}
					}
				}
				else if ( aEvent instanceof ClosedEvent ) {
					_closedEvent = true;
					synchronized ( ObservableLoopbackRemoteTest.this ) {
						ObservableLoopbackRemoteTest.this.notifyAll();
					}
				}
			};
		} );
		theClientTransceiver.open( theServerTransceiver );
		theRemoteClient.open( theClientTransceiver );
		theServerTransceiver.open( theClientTransceiver );
		theRemoteServer.open( theServerTransceiver );
		while ( !theRemoteServer.isOpened() ) {
			try {
				Thread.sleep( 100 );
			}
			catch ( InterruptedException ignored ) {}
		}
		final CountingList<String> theList = new CountingList<>();
		theList.add( "A" );
		theList.add( "B" );
		theList.add( "C" );
		assertEquals( 3, theList.getAddCount() );
		Iterator<String> e = theList.iterator();
		String ePublished;
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Before published = " + ePublished );
			}
		}
		theRemoteServer.publishSubject( theList );
		synchronized ( this ) {
			try {
				while ( _listEvent == false ) {
					wait( 300 );
				}
			}
			catch ( InterruptedException ie ) {}
		}
		e = theList.iterator();
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "After published = " + ePublished );
			}
		}
		assertTrue( theList.contains( "1" ) );
		assertTrue( theList.contains( "2" ) );
		assertTrue( theList.contains( "3" ) );
		assertTrue( theList.contains( "A" ) );
		assertTrue( theList.contains( "B" ) );
		assertTrue( theList.contains( "C" ) );
		assertEquals( 6, theList.getAddCount() );
		theRemoteServer.close();
		synchronized ( this ) {
			try {
				while ( _closedEvent == false ) {
					wait( 300 );
				}
			}
			catch ( InterruptedException e1 ) {}
		}
		theRemoteClient.close();
	}

	@Test
	public void testStressObservableLoopbackRemote() throws IOException {
		final LoopbackDatagramsTransceiver<Serializable> theClientTransceiver = new LoopbackDatagramsTransceiver<>();
		final LoopbackDatagramsTransceiver<Serializable> theServerTransceiver = new LoopbackDatagramsTransceiver<>();
		final ObservableRemoteServer theRemoteServer = new ObservableRemoteServer();
		final ObservableRemoteClient theRemoteClient = new ObservableRemoteClient();
		theRemoteClient.subscribeObserver( new AbstractConsumerObserver() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public void onEvent( ConnectionEvent<?, ObservableRemoteClient> aEvent ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "Event = " + aEvent.getClass().getName() );
				}
				if ( aEvent instanceof ProxyPublishedEvent theProxyEvent ) {
					final Object theProxy = theProxyEvent.getProxy();
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( "Proxy class = " + theProxy.getClass().getName() );
					}
					if ( theProxy instanceof List ) {
						final List<String> theProxyList = theProxyEvent.getProxy();
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( "Perforimg ADD operations:" );
						}
						for ( int i = 0; i < ITERARTIONS; i++ ) {
							if ( i % 10 == 0 ) {
								if ( SystemProperty.LOG_TESTS.isEnabled() ) {
									System.out.println( "ADD(" + i + ")" );
								}
							}
							theProxyList.add( Integer.toString( i ) );
							assertTrue( theProxyList.contains( Integer.toString( i ) ) );
						}
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( "Perforimg REMOVE operations:" );
						}
						for ( int i = 0; i < ITERARTIONS; i++ ) {
							if ( i % 10 == 0 ) {
								if ( SystemProperty.LOG_TESTS.isEnabled() ) {
									System.out.println( "REMOVE(" + i + ")" );
								}
							}
							theProxyList.remove( Integer.toString( i ) );
							assertFalse( theProxyList.contains( Integer.toString( i ) ) );
						}
						assertFalse( theProxyList.isEmpty() );
						assertTrue( theProxyList.contains( "A" ) );
						assertTrue( theProxyList.contains( "B" ) );
						assertTrue( theProxyList.contains( "C" ) );
						theProxyList.clear();
						assertTrue( theProxyList.isEmpty() );
						_listEvent = true;
						synchronized ( ObservableLoopbackRemoteTest.this ) {
							ObservableLoopbackRemoteTest.this.notifyAll();
						}
					}
				}
				else if ( aEvent instanceof ClosedEvent ) {
					_closedEvent = true;
					synchronized ( ObservableLoopbackRemoteTest.this ) {
						ObservableLoopbackRemoteTest.this.notifyAll();
					}
				}
			};
		} );
		theClientTransceiver.open( theServerTransceiver );
		theRemoteClient.open( theClientTransceiver );
		theServerTransceiver.open( theClientTransceiver );
		theRemoteServer.open( theServerTransceiver );
		while ( !theRemoteServer.isOpened() ) {
			try {
				Thread.sleep( 100 );
			}
			catch ( InterruptedException ignored ) {}
		}
		final CountingList<String> theList = new CountingList<>();
		theList.add( "A" );
		theList.add( "B" );
		theList.add( "C" );
		assertEquals( 3, theList.getAddCount() );
		Iterator<String> e = theList.iterator();
		String ePublished;
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Before published = " + ePublished );
			}
		}
		theRemoteServer.publishSubject( theList );
		synchronized ( this ) {
			try {
				while ( _listEvent == false ) {
					wait( 300 );
				}
			}
			catch ( InterruptedException ie ) {}
		}
		e = theList.iterator();
		while ( e.hasNext() ) {
			ePublished = e.next();
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "After published = " + ePublished );
			}
		}
		assertTrue( theList.isEmpty() );
		assertEquals( ITERARTIONS + 3, theList.getAddCount() );
		assertEquals( ITERARTIONS, theList.getRemoveCount() );
		assertEquals( 1, theList.getClearCount() );
		theRemoteServer.close();
		synchronized ( this ) {
			try {
				while ( _closedEvent == false ) {
					wait( 300 );
				}
			}
			catch ( InterruptedException e1 ) {}
		}
		theRemoteClient.close();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	public class CountingList<T> extends ArrayList<T> implements List<T>, Serializable {
		private static final long serialVersionUID = 1L;

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private int _addCount = 0;
		private int _removeCount = 0;
		private int _clearCount = 0;

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////
		/**
		 * @InheritDoc
		 */
		@Override
		public boolean add( T o ) {
			final boolean isAdd = super.add( o );
			_addCount++;
			return isAdd;
		}

		@Override
		public void clear() {
			super.clear();
			_clearCount++;
		}

		@Override
		public boolean remove( Object o ) {
			final boolean isRemove = super.remove( o );
			_removeCount++;
			return isRemove;
		}
		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		public int getAddCount() {
			return _addCount;
		}

		public int getRemoveCount() {
			return _removeCount;
		}

		public int getClearCount() {
			return _clearCount;
		}
	}
}
